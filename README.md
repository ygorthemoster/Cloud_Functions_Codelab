# Google Codelab: Cloud Functions Codelab
This is an implementation of a Google codelab tutorial, the tutorial is freely avaliable at: https://codelabs.developers.google.com/codelabs/firebase-cloud-functions/index.html, this tutorial teaches how to improve the live chat created [here](https://gitlab.com/ygorthemoster/Firebase_Web_Codelab) to add functions running on the cloud independently of the server, this code is the final product of said tutorial

Note: I wasn't able to enable clodu vision due to billing problems if you wish to follow that step it is available [here](https://codelabs.developers.google.com/codelabs/firebase-cloud-functions/index.html?index=..%2F..%2Findex#8)

## Pre-requisites
To test this project you'll need
- [git](https://git-scm.com/)
- [firebase-tool](https://github.com/firebase/firebase-tools)

## Installing

First you'll need to create a new firebase project, this can be via the [firebase console](https://console.firebase.google.com/), after creating it press the **Add Firebase to your web app** button and copy the part that reads
```javascript
var config = {
    ...
};
```
You'll also have to enable google auth, it also can be done at the [firebase console](https://console.firebase.google.com/project/_/authentication/providers)

then run the following on a terminal:
```
git clone https://ygorthemoster@gitlab.com/ygorthemoster/Cloud_Functions_Codelab.git
cd Cloud_Functions_Codelab/src
firebase login
firebase use --add
firebase database:set / initial_messages.json
cd functions
npm install
cd ..
firebase deploy
```

## Running
You can start the app by running the following command on a terminal:
```
firebase open hosting:site
```
Or check out my deployed version [here](https://friendlychat-2-1df1c.firebaseapp.com/) 
## License
See [LICENSE](LICENSE)

## Acknowledgments
- Original source by [Firebase](https://github.com/firebase) available at: https://github.com/firebase/friendlychat
- Tutorial by [Google Codelabs](https://codelabs.developers.google.com/) available at: https://codelabs.developers.google.com/codelabs/firebase-cloud-functions/index.html